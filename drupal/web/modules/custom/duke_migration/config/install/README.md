This is just here as a placeholder.  The install directory is required
for database updates.

The reason the actual install files are archived in `install.zip` is because we do not
actually want those configuration updates to automatically import at this point
in the project.