<?php
namespace Drupal\duke_migration\Plugin\migrate\source;

use Drupal\migrate_drupal\Plugin\migrate\source\d6\FieldableEntity;
use Drupal\node\Plugin\migrate\source\d6\Node as D6Node;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Row;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 6 node migrate source.
 *
 * Source plugin for reading Drupal 6 event nodes with extended info.
 *
 * @MigrateSource(
 *   id = "d6_node_event",
 *   source_module = "node"
 *
 * )
 */
class D6NodeEvent extends D6Node {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, EntityManagerInterface $entity_manager, ModuleHandler $module_handler) {
    $configuration['node_type'] = 'event';
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state, $entity_manager, $module_handler);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();

    $query->leftJoin('location_instance', 'i', 'i.nid = n.nid AND i.vid = n.vid');
    $query->leftJoin('location', 'l', 'l.lid = i.lid');
    $query->fields('l', [
      'name',
      'street',
      'additional',
      'city',
      'province',
      'postal_code',
      'country',
    ]);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    $fields['location'] = $this->t('Event Location');
    $fields['uploads'] = $this->t('Uploads');
    $fields['groups'] = $this->t('Groups');
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $row->setSourceProperty('location', [
      'name' => $row->getSourceProperty('name'),
      'street' => $row->getSourceProperty('street'),
      'additional' => $row->getSourceProperty('additional'),
      'city' => $row->getSourceProperty('city'),
      'province' => $row->getSourceProperty('province'),
      'postal_code' => $row->getSourceProperty('postal_code'),
      'country' => $row->getSourceProperty('country'),
    ]);

    $upload_query = $this->select('upload', 'u')
      ->fields('u', ['fid', 'description', 'list'])
      ->condition('u.nid', $row->getSourceProperty('nid'))
      ->orderBy('u.weight');
    $upload_query->innerJoin('node', 'n', 'n.nid = u.nid AND n.vid = u.vid');
    $row->setSourceProperty('uploads', $upload_query->execute()->fetchAll());

    $og_query = $this->select('og_ancestry', 'o')
      ->fields('o', ['group_nid'])
      ->condition('o.nid', $row->getSourceProperty('nid'));
    $row->setSourceProperty('groups', $og_query->execute()->fetchAll());

    return parent::prepareRow($row);
  }

}
