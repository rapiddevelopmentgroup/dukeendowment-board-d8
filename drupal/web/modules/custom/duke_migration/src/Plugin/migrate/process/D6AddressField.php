<?php

namespace Drupal\duke_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Maps D6 addressfield values to address values.
 *
 * @MigrateProcessPlugin(
 *   id = "d6_addressfield",
 *   handle_multiples = TRUE
 * )
 */
class D6AddressField extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $parsed = [
      'country_code' => 'US',
      'administrative_area' => $value['province'],
      'locality' => $value['city'],
      'postal_code' => $value['postal_code'],
      'address_line1' => $value['street'],
      'address_line2' => $value['additional'],
      'organization' => $value['name'],
    ];

    return $parsed;
  }

}
