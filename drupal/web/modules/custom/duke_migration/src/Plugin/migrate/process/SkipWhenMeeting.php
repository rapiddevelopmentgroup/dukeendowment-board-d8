<?php

namespace Drupal\duke_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * If the source evaluates to a configured value, skip processing or whole row.
 *
 * @MigrateProcessPlugin(
 *   id = "skip_when_meeting"
 * )
 *
 * Available configuration keys:
 * - inverse: (optional).
 *
 * @codingStandardsIgnoreStart
 *
 * Example:
 *
 * @code
 *   type:
 *     plugin: skip_when_meeting
 *     inverse: true
 *     source: field_board_reference
 * @endcode
 *
 * @codingStandardsIgnoreEnd
 */
class SkipWhenMeeting extends ProcessPluginBase {

  protected $TDEBoardId = 2;

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $inverse = isset($this->configuration['inverse']) && $this->configuration['inverse'];
    // $group_nid = $row->getSourceProperty('group_nid');
    $flagged = FALSE;
    foreach ($value as $item) {
      if ($item['nid'] == $this->TDEBoardId) {
        $flagged = TRUE;
      }
    }
    if (($flagged && !$inverse) || (!$flagged && $inverse)) {
      throw new MigrateSkipRowException('Skipping Row with The Duke Endowment Board association.');
    }

    return $value;
  }

}
