<?php

namespace Drupal\duke_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Determine best categorization for a resource.
 *
 * @MigrateProcessPlugin(
 *   id = "categorize_resource",
 *   handle_multiples = TRUE
 * )
 */
class CategorizeResource extends ProcessPluginBase {

  protected $categoryTerms = [
    1 => [
      'board book',
    ],
    2 => [
      'committee book',
    ],
    3 => [
      'schedule',
    ],
    4 => [
      'budget',
    ],
    5 => [
      'report',
    ],
    6 => [
      'presentation',
    ],
    7 => [
      'transportation',
    ],
    8 => [
      'hotel',
    ],
  ];

  /**
   * {@inheritdoc}
   *
   * The $value parameter should contain the text to parse for related category.
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Default to 'other'.
    $catg_tid = 9;

    drush_print($value);
    foreach ($this->categoryTerms as $tid => $search_words) {
      foreach ($search_words as $word) {
        $clean_value = str_replace(['_', '-'], ' ', $value);
        if (stripos($clean_value, $word) !== FALSE) {
          $catg_tid = $tid;
        }
      }
    }
    drush_print($catg_tid);
    return $catg_tid;
  }

}
