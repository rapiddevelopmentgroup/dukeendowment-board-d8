<?php

namespace Drupal\duke_migration\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\migrate\MigrateSkipProcessException;

/**
 * This plugin looks for existing MEETING entities.
 *
 * Uses introspection of sorts to get the necessary info to find a meeting
 *   based on an D6 event node.
 *
 * @codingStandardsIgnoreStart
 *
 * Example usage with minimal configuration:
 * @code
 * destination:
 *   plugin: 'entity:node'
 * process:
 *   type:
 *     plugin: meeting_entity_lookup
 *     source: field_field
 * @endcode
 * In this example above, the access check is disabled.

 * @codingStandardsIgnoreEnd
 *
 * @MigrateProcessPlugin(
 *   id = "meeting_entity_lookup",
 * )
 */
class MeetingEntityLookup extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  protected $defaultMeetingID = 17;
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The migration.
   *
   * @var \Drupal\migrate\Plugin\MigrationInterface
   */
  protected $migration;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MigrationInterface $migration, EntityManagerInterface $entityManager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->migration = $migration;
    $this->entityManager = $entityManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $migration,
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    if (!isset($value['value'])) {
      return NULL;
    }
    $start_date = DrupalDateTime::createFromFormat('Y-m-d H:i:s', $value['value'])->format('Y-m-d');
    $query = $this->entityManager->getStorage('node')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'meeting')
      ->condition('field_meeting_date.value', $start_date, '<=')
      ->condition('field_meeting_date.end_value', $start_date, '>=')
      ->addTag('debug');
    $results = $query->execute();
    if (empty($results)) {
      return $this->defaultMeetingID;
    }
    return reset($results);
  }

}
