<?php

namespace Drupal\duke_migration\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\file\Plugin\migrate\source\d6\File;

/**
 * Drupal 6 file source from database.
 *
 * @MigrateSource(
 *   id = "d6_duke_file",
 *   source_module = "system"
 * )
 */
class D6DukeFile extends File {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = parent::query();
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $upload_query = $this->select('upload', 'u')
      ->fields('u', ['description', 'list'])
      ->condition('u.fid', $row->getSourceProperty('fid'))
      ->orderBy('u.weight');
    $upload_query->innerJoin('node', 'n', 'n.nid = u.nid AND n.vid = u.vid');
    $upload = $upload_query->execute()->fetchObject();
    $row->setSourceProperty('upload_description', $upload->description ? $upload->description : $row->getSourceProperty('filename'));
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = parent::fields();
    $fields['upload_description'] = $this->t('Upload Description');
    return $fields;
  }

}
