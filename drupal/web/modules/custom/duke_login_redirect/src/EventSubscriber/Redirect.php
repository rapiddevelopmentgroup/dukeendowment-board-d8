<?php

namespace Drupal\duke_login_redirect\EventSubscriber;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\openid_connect\OpenIDConnectClaims;
use Drupal\openid_connect\OpenIDConnectSession;
use Drupal\openid_connect\Plugin\OpenIDConnectClientManager;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * Event Subscriber Redirect.
 */
class Redirect extends HttpExceptionSubscriberBase {

  /**
   * Kill cache switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $cacheKillSwitch;

  /**
   * OpenID connection session.
   *
   * @var \Drupal\openid_connect\OpenIDConnectSession
   */
  protected $openIdSession;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $account;

  /**
   * Drupal\openid_connect\Plugin\OpenIDConnectClientManager definition.
   *
   * @var \Drupal\openid_connect\Plugin\OpenIDConnectClientManager
   */
  protected $clientManager;

  /**
   * The OpenID Connect claims.
   *
   * @var \Drupal\openid_connect\OpenIDConnectClaims
   */
  protected $claims;

  /**
   * Drupal current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */

  protected $routeMatch;

  /**
   * Creates a new Redirect subscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\openid_connect\Plugin\OpenIDConnectClientManager $clients
   *   The OpenID Client manager.
   * @param \Drupal\openid_connect\OpenIDConnectClaims $claims
   *   The OpenID Connect Claims service.
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   The page cache kill switch.
   * @param \Drupal\openid_connect\OpenIDConnectSession $open_session
   *   The OpendId connection session.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   The current route match service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AccountInterface $account,
    OpenIDConnectClientManager $clients,
    OpenIDConnectClaims $claims,
    KillSwitch $kill_switch,
    OpenIDConnectSession $open_session,
    CurrentRouteMatch $route_match,
  ) {
    $this->configFactory = $config_factory;
    $this->account = $account;
    $this->clientManager = $clients;
    $this->claims = $claims;
    $this->cacheKillSwitch = $kill_switch;
    $this->openIdSession = $open_session;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats() {
    return ['html'];
  }

  /**
   * Redirects on 403 Access Denied kernel exceptions.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The Event to process.
   */
  public function on403(ExceptionEvent $event) {
    $is_client_callback = ($this->routeMatch->getRouteName() === 'openid_connect.redirect_controller_redirect') ? TRUE : FALSE;

    if (!$is_client_callback && $this->account->isAnonymous()) {
      // While avoiding infinite redirects if Drupal authentication fails b/c
      // 1) site visitors can not create new accounts, 2) authentication with
      // the OpenID Connect client fails, etc.
      $this->openIdSession->saveDestination();
      $client_name = 'windows_aad';
      $client_settings = $this->configFactory->get('openid_connect.client.' . $client_name)->get('settings');

      $client = $this->clientManager->createInstance(
        $client_name,
        $client_settings
      );
      $scopes = $this->claims->getScopes();
      $_SESSION['openid_connect_op'] = 'login';
      $this->cacheKillSwitch->trigger();
      $client->setParentEntityId('windows_aad');
      $response = $client->authorize($scopes);

      // Add caching dependencies so the cache of the redirection will be
      // updated when necessary.
      $cacheableMetadata = new CacheableMetadata();
      // Add original 403 response cache metadata.
      $cacheableMetadata->addCacheableDependency($event->getThrowable());
      // We still need to add the client error tag manually since the core
      // wil not recognize our redirection as an error.
      $cacheableMetadata->addCacheTags(['4xx-response']);
      // Attach cache metadata to the response.
      $response->addCacheableDependency($cacheableMetadata);

      $event->setResponse($response);
    }

  }

  /**
   * {@inheritdoc}
   */
  protected static function getPriority() {
    // A very low priority so that custom handlers are almost certain to fire
    // before it, even if someone forgets to set a priority.
    return 100;
  }

}
