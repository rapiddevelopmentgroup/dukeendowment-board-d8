/**
 * @file filter-resource-categories.js
 *
 * Filter the categories available to user based on parent node.
 */
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.filterResourceCategories = {
    attach: function (context, settings) {
      $('.field--name-field-resource-category select option', context).each(function () {
        var $option = $(this);
        if ($.inArray($option.attr('value'), Object.values(settings.resourceCategoryWhitelist)) < 0) {
          $option.remove();
        }
      });
    }
  };

})(jQuery, Drupal);
