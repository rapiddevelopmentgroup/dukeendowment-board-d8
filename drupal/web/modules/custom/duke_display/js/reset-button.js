/**
 * @file reset-button.js
 *
 * Support Ajax-loaded views with a reset filter button (BEF).
 */
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.resetButton = {
    attach: function (context, settings) {
      $('.form-submit[id^=edit-reset]', context).on('click keypress', function (e) {
        e.preventDefault();
        location.href = location.origin + location.pathname;
      });
    }
  };

})(jQuery, Drupal);
