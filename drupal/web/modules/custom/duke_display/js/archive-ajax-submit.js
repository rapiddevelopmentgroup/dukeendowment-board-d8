/**
 * @file archive-ajax-submit.js
 *
 * Support Ajax-loaded views using the jquery SELECTABILITY plugin.
 */
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.archiveAjaxSubmit = {
    attach: function (context, settings) {
      $('.archive-form #edit-submit').hide();
      $('select.selectability-offscreen', context).on('change', function (e) {
        $(this).closest('form').find('#edit-submit').click();
      });
    }
  };

})(jQuery, Drupal);
