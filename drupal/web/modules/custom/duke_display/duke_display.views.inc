<?php

/**
 * Implements hook_views_data().
 */
function duke_display_views_data() {
  $data = [];
  $data['node__field_session_datetime']['meeting_year_filter'] = [
    'title' => t('Filter meetings by year only.'),
    'filter' => [
      'title' => t('Meeting Year Filter'),
      'group' => 'Custom',
      'help' => t('Filters meetings by the year only.'),
      'field' => 'field_session_datetime_value',
      'id' => 'meeting_year_filter',
    ],
  ];
  $data['node__field_session_date']['meeting_year_filter_range'] = [
    'title' => t('Filter meetings by year only (Range).'),
    'filter' => [
      'title' => t('Meeting Year Filter (Range)'),
      'group' => 'Custom',
      'help' => t('Filters meetings by the year only.'),
      'field' => 'field_session_date_value',
      'id' => 'meeting_year_filter',
    ],
  ];
  $data['media__field_resource_date']['meeting_year_filter'] = [
    'title' => t('Filter resources by year only.'),
    'filter' => [
      'title' => t('Resource Year Filter'),
      'group' => 'Custom',
      'help' => t('Filters resources by the year only.'),
      'field' => 'field_resource_date_value',
      'id' => 'meeting_year_filter',
    ],
  ];
  return $data;
}
