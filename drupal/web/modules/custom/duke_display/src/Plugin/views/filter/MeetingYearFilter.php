<?php

namespace Drupal\duke_display\Plugin\views\filter;

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;

/**
 * Filters meetings by year only.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("meeting_year_filter")
 */
class MeetingYearFilter extends InOperator {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->valueTitle = t('Year');
    $this->definition['options callback'] = [$this, 'generateOptions'];
  }

  /**
   * Override the query so that no filtering takes place when empty.
   */
  public function query() {
    if (empty($this->value)) {
      return;
    }
    $this->ensureMyTable();
    $placeholder = $this->placeholder();
    $value_array = is_array($this->value) ? $this->value : [];
    $this->query->addWhereExpression($this->options['group'], "YEAR($this->tableAlias.$this->realField) IN ($placeholder)",
      [$placeholder => implode(',', array_values($value_array))]
    );
  }

  /**
   * Skip validation if no options have been chosen.
   */
  public function validate() {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * Helper function that generates the year options.
   *
   * @return array
   *   Option list.
   */
  public function generateOptions() {
    $years = array_reverse(range(2007, date('Y')));
    $year_options = array_combine($years, $years);
    return $year_options;
  }

}
