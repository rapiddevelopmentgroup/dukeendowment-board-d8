<?php

namespace Drupal\duke_display\Plugin\Menu;

use Drupal\Core\Menu\LocalTaskDefault;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;

/**
 * Provides customizations to the Sessions meeting node tab.
 */
class SessionsTab extends LocalTaskDefault {

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $route_match) {
    $node_parameter = $route_match->getParameter('node');
    $nid = ($node_parameter instanceof NodeInterface) ? $node_parameter->id() : $node_parameter;
    return ['node' => $nid];
  }

}
