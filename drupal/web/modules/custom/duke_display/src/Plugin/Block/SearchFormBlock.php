<?php

namespace Drupal\duke_display\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SearchFormBlock' block.
 *
 * @Block(
 *  id = "search_form_block",
 *  admin_label = @Translation("Search Form Block"),
 * )
 */
class SearchFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('\Drupal\duke_display\Form\SearchForm');
    return $form;
  }

}
