<?php

namespace Drupal\duke_display\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'MeetingActionsBlock' block.
 *
 * @Block(
 *  id = "meeting_actions_block",
 *  admin_label = @Translation("Meeting Actions Block"),
 * )
 */
class MeetingActionsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Routing\RedirectDestinationInterface definition.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * Constructs a new MeetingActionsBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManagerInterface definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    RedirectDestinationInterface $redirect_destination
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->redirectDestination = $redirect_destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('redirect.destination')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => 'meeting_actions_block',
      '#cache' => ['contexts' => ['url.path', 'url.query_args']],
    ];

    $node = \Drupal::service('duke_display.default')->ensureMeetingNode();
    if ($node instanceof NodeInterface && $node->getType() == 'meeting') {

      $default_options = [
        '#type' => 'link',
        '#options' => [
          'absolute' => TRUE,
          'base_url' => $GLOBALS['base_url'],
        ],
      ];
      $links = [
        $default_options + [
          '#url' => Url::fromRoute('node.add', [
            'node_type' => 'session',
          ], [
            'query' => [
              'meeting' => $node->id(),
              'destination' => $this->redirectDestination->get(),
            ],
          ]),
          '#title' => t('Create a new Session'),
        ],
        $default_options + [
          '#url' => Url::fromRoute('duke_rsvp.create_rsvp_form', [
            'node' => $node->id(),
          ], [
            'query' => [
              'destination' => $this->redirectDestination->get(),
            ],
          ]),
          '#title' => t('Create RSVP Form'),
        ],
      ];

      $build['meeting_actions_block'] = [
        '#prefix' => '<div id="node-actions">',
        '#theme' => 'item_list',
        '#items' => $links,
        '#suffix' => '</div>',
      ];

    }
    else {
      $build['meeting_actions_block']['#markup'] = 'Meeting actions are not compatible with this page view.';
    }
    return $build;
  }

  /**
   * Indicates whether the block should be shown.
   *
   * Blocks with specific access checking should override this method rather
   * than access(), in order to avoid repeating the handling of the
   * $return_as_object argument.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user session for which to check access.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   *
   * @see self::access()
   */
  protected function blockAccess(AccountInterface $account) {
    $node = \Drupal::service('duke_display.default')->ensureMeetingNode();
    ;

    if ($account->hasPermission('edit any meeting content')) {
      return AccessResult::allowedIf(in_array(\Drupal::routeMatch()->getRouteName(), [
        'node.view',
        'entity.node.canonical',
      ]) && $node->getType() == 'meeting');
    }
    return AccessResult::forbidden();
  }

}
