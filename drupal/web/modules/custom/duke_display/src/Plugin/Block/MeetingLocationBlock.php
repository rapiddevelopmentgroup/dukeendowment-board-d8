<?php

namespace Drupal\duke_display\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Site\Settings;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'MeetingLocationBlock' block.
 *
 * @Block(
 *  id = "meeting_location_block",
 *  admin_label = @Translation("Meeting Location Block"),
 * )
 */
class MeetingLocationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new MeetingLocationBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The CurrentRouteMatch definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManagerInterface definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    CurrentRouteMatch $current_route_match,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $current_route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'display_map_image' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['display_map_image'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display map image'),
      '#description' => $this->t('Note: Billing must be enabled on the <a href="https://console.cloud.google.com/project/_/billing/enable">Google Cloud Project</a>. <a href="https://developers.google.com/maps/gmp-get-started">Learn more here</a>.'),
      '#default_value' => $config['display_map_image'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['display_map_image'] = $form_state->getValue('display_map_image');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $meeting_node = $this->currentRouteMatch->getParameter('node');
    $display_map_image = $this->configuration['display_map_image'];

    if ($this->currentRouteMatch->getRouteName() == 'view.dashboard.page_1') {
      $view = Views::getView('dashboard');
      $view->setDisplay('page_1');
      $view->preExecute();
      $view->execute();
      $viewContent = $view->buildRenderable('page_1');
      $meeting_node_id = $viewContent['#view']->result[0]->nid;

      $meeting_node = \Drupal::entityTypeManager()->getStorage('node')->load($meeting_node_id);
    }

    if ($meeting_node && isset($meeting_node->field_meeting_location)) {
      $address = $meeting_node->field_meeting_location->getValue();
      $address = reset($address);
      if (!$address || !isset($address['address_line1'])) {
        return [];
      }
      $location_string = implode(',', [
        $address['address_line1'],
        $address['locality'],
        $address['administrative_area'],
        $address['postal_code'],
      ]);

      $address_items = [];
      if ($address['organization']) {
        $address_items[] = $address['organization'];
      }
      if ($address['address_line1']) {
        $address_items[] = $address['address_line1'];
      }
      if ($address['address_line2']) {
        $address_items[] = $address['address_line2'];
      }
      $address_items[] = $address['locality'] . ', ' . $address['administrative_area'] . ($address['postal_code'] ? (', ' . $address['postal_code']) : '');
      $build = [
        '#title' => 'Meeting Location w/ Map',
        '#theme' => 'meeting_location_block',
        '#description' => 'Displays meeting location info, including a Google Map with link to directions.',
        '#location' => $address_items,
        '#gmap_formatted_url' => 'https://maps.googleapis.com/maps/api/staticmap?center=' . urlencode($location_string) . '&zoom=15&size=800x400&key=' . Settings::get('googlemaps_api_key'),
        '#display_map_image' => $display_map_image,
      ];
      return $build;
    }
    else {
      return [];
    }

  }

}
