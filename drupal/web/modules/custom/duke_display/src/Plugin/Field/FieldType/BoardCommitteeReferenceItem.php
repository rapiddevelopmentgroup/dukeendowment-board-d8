<?php

namespace Drupal\duke_display\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Variant of the 'link' field that links to the current company.
 *
 * @FieldType(
 *   id = "board_committee",
 *   label = @Translation("Board or Committee Association"),
 *   description = @Translation("A reference to the group this resource belongs to."),
 *   default_widget = "entity_reference_autocomplete",
 *   default_formatter = "entity_reference_label",
 * )
 */
class BoardCommitteeReferenceItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'node',
    ] + parent::defaultStorageSettings();
  }

  /**
   * Whether or not the value has been calculated.
   *
   * @var bool
   */
  protected $isCalculated = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __get($name) {
    $this->ensureCalculated();
    return parent::__get($name);
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $this->ensureCalculated();
    return parent::isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $this->ensureCalculated();
    return parent::getValue();
  }

  /**
   * Calculates the value of the field and sets it.
   */
  protected function ensureCalculated() {
    if (!$this->isCalculated) {
      $entity = $this->getEntity();
      if (!$entity->isNew()) {
        $group_nodes = \Drupal::service('duke_display.default')->groupsForResource($this->getEntity());
        $value = NULL;
        if ($group_nodes) {
          $value = [
            'target_id' => reset($group_nodes),
          ];
        }
        $this->setValue($value);
      }
      $this->isCalculated = TRUE;
    }
  }

}
