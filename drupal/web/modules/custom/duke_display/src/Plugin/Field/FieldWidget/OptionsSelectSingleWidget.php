<?php

namespace Drupal\duke_display\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'options_select_single' widget.
 *
 * @FieldWidget(
 *   id = "options_select_single",
 *   label = @Translation("Select list (Single Only)"),
 *   field_types = {
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string"
 *   },
 *   multiple_values = FALSE
 * )
 */
class OptionsSelectSingleWidget extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['#default_value'] = isset($items[$delta]->{$this->column}) ? $items[$delta]->{$this->column} : NULL;
    $element['#multiple'] = FALSE;
    $element['#element_validate'] = [];
    $element['#element_validate'][] = [get_class($this), 'validateElement'];

    return [$this->column => $element];
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if ($element['#required'] && $element['#value'] == '_none') {
      $form_state->setError($element, t('@name field is required.', ['@name' => $element['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values = array_filter($values, function ($value) {
      return $value[$this->column] !== '_none';
    });
    return $values;
  }

}
