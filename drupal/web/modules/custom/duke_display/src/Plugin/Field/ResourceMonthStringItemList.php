<?php

namespace Drupal\duke_display\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Item list for a computed field that displays the created month.
 */
class ResourceMonthStringItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Compute the list property from state.
   */
  protected function computeValue() {
    /** @var \Drupal\media\Entity\Media $entity */
    $entity = $this->getEntity();
    if ($entity->getEntityTypeId() != 'media') {
      return;
    }
    $month = date('m', $entity->getCreatedTime());
    $this->list = [
      $this->createItem(0, $month),
    ];
  }

}
