<?php

namespace Drupal\duke_display\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Item list for a computed field that finds the related committee or board.
 */
class BoardCommitteeAssociationReferenceItemList extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  /**
   * Compute the list property from state.
   */
  protected function computeValue() {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->getEntity();
    $group_nodes = \Drupal::service('duke_display.default')->groupsForResource($entity);
    if ($group_nodes) {
      $this->list = [
        $this->createItem(0, reset($group_nodes)),
      ];
    }
  }

}
