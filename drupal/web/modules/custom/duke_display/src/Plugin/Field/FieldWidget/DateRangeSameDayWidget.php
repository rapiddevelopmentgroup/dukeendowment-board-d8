<?php

namespace Drupal\duke_display\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeDefaultWidget;

/**
 * Plugin implementation of the 'daterange_default' widget.
 *
 * @FieldWidget(
 *   id = "daterange_same_day",
 *   label = @Translation("Date and time range (Same Day)"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class DateRangeSameDayWidget extends DateRangeDefaultWidget implements ContainerFactoryPluginInterface, TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['#element_validate'] = [[$this, 'validateStartEnd']];

    // Identify the type of date and time elements to use.
    switch ($this->getFieldSetting('datetime_type')) {
      case DateRangeItem::DATETIME_TYPE_DATE:
      case DateRangeItem::DATETIME_TYPE_ALLDAY:
        $date_type = 'date';
        $time_type = 'none';
        $date_format = $this->dateStorage->load('html_date')->getPattern();
        $time_format = '';
        break;

      default:
        $date_type = 'date';
        $time_type = 'time';
        $date_format = $this->dateStorage->load('html_date')->getPattern();
        $time_format = $this->dateStorage->load('html_time')->getPattern();
        break;
    }

    // Widget types: 'datetime'.
    $element['value'] = [
      '#title' => NULL,
      '#title_display' => 'invisible',
      '#date_date_format' => $date_format,
      '#date_date_element' => $date_type,
      '#date_date_callbacks' => [],
      '#date_time_format' => $time_format,
      '#date_time_element' => $time_type,
      '#date_time_callbacks' => [[$this, 'startTimeCallback']],
    ] + $element['value'];

    $element['end_value'] = [
      '#title' => NULL,
      '#title_display' => 'invisible',
      '#date_date_format' => '',
      '#date_date_element' => 'none',
      '#date_date_callbacks' => [],
      '#date_time_format' => $time_format,
      '#date_time_element' => $time_type,
      '#date_time_callbacks' => [[$this, 'endTimeCallback']],
    ] + $element['end_value'];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values = parent::massageFormValues($values, $form, $form_state);
    $start = strtotime($values[0]['value']);
    $end = strtotime($values[0]['end_value']);
    $values[0]['end_value'] = date("Y-m-d\T", $start) . date("H:i:s", $end);
    return $values;
  }

  /**
   * Element_validate callback to ensure that the start date <= the end date.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public function validateStartEnd(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $start_date = $element['value']['#value']['object'];
    $end_date = $element['end_value']['#value']['object'];

    if ($start_date instanceof DrupalDateTime && $end_date instanceof DrupalDateTime) {
      if ($start_date->getTimestamp() !== $end_date->getTimestamp()) {
        $interval = $start_date->diff($end_date);
        if ($interval->invert === 1) {
          $element['end_value']['#value']['object'] = DrupalDateTime::createFromTimestamp($start_date->getTimestamp() + 3600);
        }
      }
    }
  }

  /**
   * Title modifications, etc, for time part of datetime field.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $date
   *   The date value.
   *
   * @see \Drupal\webform\Plugin\WebformElement\DateTime::prepare
   */
  public static function endTimeCallback(array &$element, FormStateInterface $form_state, DrupalDateTime $date = NULL) {
    $element['#title'] = t('End Time');
    $element['#title_display'] = 'before';
  }

  /**
   * Title modifications, etc, for time part of datetime field.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $date
   *   The date value.
   *
   * @see \Drupal\webform\Plugin\WebformElement\DateTime::prepare
   */
  public static function startTimeCallback(array &$element, FormStateInterface $form_state, DrupalDateTime $date = NULL) {
    $element['#title'] = t('Start');
    $element['#title_display'] = 'before';
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['endTimeCallback', 'startTimeCallback'];
  }

}
