<?php

namespace Drupal\duke_display\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\link\Plugin\Field\FieldType\LinkItem;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'resource_title' formatter.
 *
 * @FieldFormatter(
 *   id = "resource_title",
 *   label = @Translation("Resource Title Link"),
 *   field_types = {
 *     "file",
 *     "image",
 *     "link"
 *   }
 * )
 */
class ResourceTitleFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays file with the title of the media entity.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $parent = $items->getParent()->getValue()->id();

    foreach ($items as $delta => $item) {

      $route_parameters = ['media' => $parent];
      if ($delta > 0) {
        $route_parameters['query']['delta'] = $delta;
      }

      if ($item instanceof LinkItem) {
        $url = $item->getUrl();
      }
      else {
        $url = Url::fromRoute('media_entity_download.download', $route_parameters);
      }

      $elements[$delta] = [
        '#type' => 'link',
        '#url' => $url,
        '#title' => $item->entity_name,
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {
    foreach ($entities_items as $items) {
      foreach ($items as $item) {
        if (!$item->isEmpty()) {
          $item->entity_name = $item->getEntity()->getName();
        }
      }
    }
  }

}
