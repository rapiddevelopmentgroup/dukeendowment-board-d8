<?php

namespace Drupal\duke_display\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Item list for a computed field that displays the created Year.
 */
class ResourceYearStringItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Compute the list property from state.
   */
  protected function computeValue() {
    /** @var \Drupal\media\Entity\Media $entity */
    $entity = $this->getEntity();
    if ($entity->getEntityTypeId() != 'media') {
      return;
    }
    $year = date('Y', $entity->getCreatedTime());
    $this->list = [
      $this->createItem(0, $year),
    ];
  }

}
