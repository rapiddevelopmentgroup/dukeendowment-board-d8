<?php

namespace Drupal\duke_display\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Item list for a computed field that displays the created month.
 *
 * @see \Drupal\duke_display\Plugin\Field\FieldType\CreatedMonthStringItem().
 */
class CreatedMonthStringItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * Compute the list property from state.
   */
  protected function computeValue() {
    if (!isset($this->list[0])) {
      $this->list[0] = $this->createItem(0);
    }
  }

}
