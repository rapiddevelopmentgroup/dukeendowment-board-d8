<?php

namespace Drupal\duke_display\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('view.sessions_by_meeting.page_1')) {
      $route->setRequirement('_custom_access', '\Drupal\duke_display\Controller\MeetingAccessController::access');
    }
    if ($route = $collection->get('entity.webform.settings')) {
      $route->setRequirement('_custom_access', '\Drupal\duke_display\Controller\WebformSettingsAccessController::access');
    }
  }

}
