<?php

namespace Drupal\duke_display\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ArchiveForm.
 */
class ArchiveForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'archive_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'duke_display/views.selectability';
    $form['filters'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'filters',
      ],
    ];

    $years = array_reverse(range(2007, date('Y')));
    $year_options = array_combine($years, $years);
    $year_options = ['all' => $this->t('– Any –')] + $year_options;
    $form['filters']['year'] = [
      '#type' => 'select',
      '#options' => $year_options,
      '#title' => $this->t('Year'),
      '#weight' => '-10',
      '#default_value' => 'all',
      '#tree' => FALSE,
    ];

    $form['filters']['month'] = [
      '#type' => 'select',
      '#title' => $this->t('Month'),
      '#options' => [
        'all'  => $this->t('– Any –'),
        '01' => $this->t('January'),
        '02' => $this->t('February'),
        '03' => $this->t('March'),
        '04' => $this->t('April'),
        '05' => $this->t('May'),
        '06' => $this->t('June'),
        '07' => $this->t('July'),
        '08' => $this->t('August'),
        '09' => $this->t('September'),
        '10' => $this->t('October'),
        '11' => $this->t('November'),
        '12' => $this->t('December'),
      ],
      '#tree' => FALSE,
      '#weight' => '-5',
      '#default_value' => 'all',
    ];

    $form['filters']['reset_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'edit-reset',
      ],
    ];

    $form['filters']['reset_container']['reset'] = [
      '#type' => 'button',
      '#value' => 'Clear all filters',
      '#attributes' => [
        'onclick' => 'window.location.href = window.location.href; return false;',
      ],
    ];

    $form['filters']['sort'] = [
      '#type' => 'radios',
      '#title' => $this->t('Sort by'),
      '#options' => [
        'desc' => $this->t('Newest'),
        'asc' => $this->t('Oldest'),
      ],
      '#default_value' => 'desc',
      '#ajax' => [
        'callback' => '::refreshPage',
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'archive-results',
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#ajax' => [
        'callback' => '::refreshPage',
        'event' => 'click',
        'wrapper' => 'archive-results',
      ],
    ];

    $current_input = $form_state->getUserInput();

    $args = [];
    if (isset($current_input['year']) && $year = $current_input['year']) {
      $args[] = $year;
    }
    else {
      $args[] = 'all';
    }
    if (isset($current_input['month']) && $month = $current_input['month']) {
      $args[] = $month;
    }
    else {
      $args[] = 'all';
    }
    $display_id = isset($current_input['sort']) && $current_input['sort'] == 'asc' ? 'embed_1' : 'embed';

    $form['results'] = [
      '#type' => 'view',
      '#name' => 'archived_meetings',
      '#display_id' => $display_id,
      '#arguments' => $args,
      '#attributes' => [
        'id' => ['archive-results'],
      ],
    ];

    $form['#prefix'] = '<div id="archive-form-wrapper">';
    $form['#suffix'] = '</div>';

    return $form;
  }

  /**
   * Ajax Callback for filtering.
   */
  public function refreshPage(array &$form, FormStateInterface $form_state) {
    // $form_state->setRebuild();
    return $form['results'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
