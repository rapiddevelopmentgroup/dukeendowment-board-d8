<?php

namespace Drupal\duke_display\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Class EditResourceController.
 */
class EditResourceController extends ControllerBase {

  /**
   * Returns a page title.
   */
  public function getTitle() {
    $node = \Drupal::service('duke_display.default')->ensureMeetingNode();
    return $this->t('Edit Resources: @title', ['@title' => $node->getTitle()]);
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    $node = \Drupal::service('duke_display.default')->ensureMeetingNode();
    $node_access = AccessResult::allowedIf(isset($node) && ($node->getType() == 'meeting' || $node->getType() == 'session'));
    return AccessResult::allowedIf($node_access == AccessResult::allowed() && $account->hasPermission('edit node resources'));
  }

}
