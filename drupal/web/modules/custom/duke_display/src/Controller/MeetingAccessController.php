<?php

namespace Drupal\duke_display\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Class MeetingAccessController.
 */
class MeetingAccessController extends ControllerBase {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    $node = \Drupal::service('duke_display.default')->ensureMeetingNode();
    return AccessResult::allowedIf(isset($node) && $node->getType() == 'meeting');
  }

}
