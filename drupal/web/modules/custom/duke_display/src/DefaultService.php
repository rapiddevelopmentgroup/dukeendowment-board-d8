<?php

namespace Drupal\duke_display;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\media\MediaInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Class DefaultService.
 */
class DefaultService {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Routing\RouteMatchInterface definition.
   *
   * @var Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new DefaultService object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $routeMatch) {
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $routeMatch;
  }

  /**
   * Retrieves a meeting's sessions.
   *
   * @param \Drupal\node\NodeInterface $meeting_node
   *   Meeting to find sessions for.
   *
   * @return array
   *   Session Nodes.
   */
  public function sessionsForMeeting(NodeInterface $meeting_node) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $query = $node_storage->getQuery();
    $nids = $query->condition('type', 'session')
      ->condition('field_session_meeting', $meeting_node->id())
      ->condition('status', '1')
      ->accessCheck(FALSE)
      ->execute();
    return $node_storage->loadMultiple($nids);
  }

  /**
   * Looks for a node in the current route and ensures it's a node object.
   */
  public function ensureMeetingNode() {
    $node_parameter = $this->routeMatch->getParameter('node');
    if ($node_parameter instanceof NodeInterface) {
      return $node_parameter;
    }
    return $this->entityTypeManager->getStorage('node')->load($node_parameter);
  }

  /**
   * Looks for a node in the current route and ensures it's a NID string.
   */
  public function ensureMeetingNid() {
    $node_parameter = $this->routeMatch->getParameter('node');
    $nid = ($node_parameter instanceof NodeInterface) ? $node_parameter->id() : $node_parameter;
    return $nid;
  }

  /**
   * Return a list of node resource categories for the node type given.
   */
  public function resourceCategories($node_type) {
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $query = $term_storage->getQuery();
    $results = $query->condition('vid', 'resource_type')
      ->condition('field_resource_type_category', $node_type)
      ->accessCheck(FALSE)
      ->execute();
    $terms = Term::loadMultiple($results);
    return $terms;
  }

  /**
   * Retrieves an option list used for selecting Committees and Boards.
   */
  public function getGroups() {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $query = $node_storage->getQuery();
    $group_nids = $query->condition('type', ['board', 'committee'], 'IN')
      ->condition('status', '1')
      ->accessCheck(FALSE)
      ->execute();

    $nodes = Node::loadMultiple($group_nids);
    $options = [];
    foreach ($nodes as $node) {
      $options[$node->id()] = $node->getTitle();
    }
    return $options;
  }

  /**
   * Retrieves a node repping a Committee/Board to which this entity belongs.
   *
   * @param \Drupal\media\MediaInterface $media_entity
   *   The media entity item.
   *
   * @return array
   *   Node IDs.
   */
  public function groupsForResource(MediaInterface $media_entity) {
    $node_storage = $this->entityTypeManager->getStorage('node');
    $query = $node_storage->getQuery();
    $group_nids = $query->condition('type', ['board', 'committee'], 'IN')
      ->condition('field_resources', $media_entity->id())
      ->condition('status', '1')
      ->accessCheck(FALSE)
      ->execute();

    $database = \Drupal::database();
    $query = $database->select('node__field_resources', 't');
    $query->condition('t.field_resources_target_id', $media_entity->id());
    $query->condition('t.bundle', 'session');
    $query->leftJoin('node__field_session_boards', 'sb', 'sb.entity_id = t.entity_id');
    $query->leftJoin('node__field_session_committees', 'sc', 'sc.entity_id = t.entity_id');
    $query->addField('sb', 'field_session_boards_target_id', 'board');
    $query->addField('sc', 'field_session_committees_target_id', 'committee');
    $result = $query->execute();

    foreach ($result as $record) {
      if (isset($record->board)) {
        $group_nids[] = $record->board;
      }
      if (isset($record->committee)) {
        $group_nids[] = $record->committee;
      }
    }

    return $group_nids;
  }

}
