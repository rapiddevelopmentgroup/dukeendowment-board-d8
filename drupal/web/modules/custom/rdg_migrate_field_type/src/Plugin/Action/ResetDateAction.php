<?php

namespace Drupal\rdg_migrate_field_type\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Action description.
 *
 * @Action(
 *   id = "rdg_migrate_field_type_reset_date_action",
 *   label = @Translation("Reset Date"),
 *   type = ""
 * )
 */
class ResetDateAction extends ViewsBulkOperationsActionBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if (isset($entity->field_session_datetime)) {
      $entity->field_session_date = [
        'value' => $entity->field_session_datetime->value . 'T09:00:00',
        'end_value' => $entity->field_session_datetime->value . 'T10:00:00',
      ];
      $entity->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object->getEntityType() === 'node') {
      $access = $object->access('update', $account, TRUE)
        ->andIf($object->status->access('edit', $account, TRUE));
      return $return_as_object ? $access : $access->isAllowed();
    }

    // Other entity types may have different
    // access methods and properties.
    return TRUE;
  }

}
