<?php

namespace Drupal\rdg_migrate_field_type\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManagerInterface;

/**
 * This plugin looks for existing session entities.
 *
 * Uses introspection of sorts to get the necessary info to find a meeting
 *   based on an D6 event node.
 *
 * @codingStandardsIgnoreStart
 *
 * Example usage with minimal configuration:
 * @code
 * destination:
 *   plugin: 'entity:node'
 * process:
 *   type:
 *     plugin: session_media_entity_lookup
 *     source: field_field
 * @endcode
 * In this example above, the access check is disabled.

 * @codingStandardsIgnoreEnd
 *
 * @MigrateProcessPlugin(
 *   id = "session_media_entity_lookup",
 * )
 */
class SessionByMediaLookup extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The migration.
   *
   * @var \Drupal\migrate\Plugin\MigrationInterface
   */
  protected $migration;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MigrationInterface $migration, EntityManagerInterface $entityManager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->migration = $migration;
    $this->entityManager = $entityManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $migration,
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    if (!$value) {
      return NULL;
    }
    $results = $this->entityManager->getStorage('node')
      ->loadByProperties([
        'type' => 'session',
        'field_resources' => $value,
        'field_session_committees' => 761, // 761 -> dumac , 762 -> NT
      ]);
    if (empty($results)) {
      return NULL;
    }
    $result = reset($results);
    return $result->field_session_date->value;
  }

}
