<?php

namespace Drupal\medialink\Plugin\media\Source;

use Drupal\link\LinkItemInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;

/**
 * Link media source.
 *
 * @see \Drupal\link\LinkItemInterface
 *
 * @MediaSource(
 *   id = "link",
 *   label = @Translation("Link"),
 *   description = @Translation("Use remote URL for reusable media."),
 *   allowed_field_types = {"link"},
 *   default_thumbnail_filename = "generic.png"
 * )
 */
class Link extends MediaSourceBase {

  /**
   * Key for "Name" metadata attribute.
   *
   * @var string
   */
  const METADATA_ATTRIBUTE_NAME = 'name';

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      static::METADATA_ATTRIBUTE_NAME => $this->t('Name'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    /** @var \Drupal\link\LinkItemInterface $link */
    $link = $media->get($this->configuration['source_field'])->entity;
    // If the source field is not required, it may be empty.
    if (!$link) {
      return parent::getMetadata($media, $attribute_name);
    }
    switch ($attribute_name) {
      case static::METADATA_ATTRIBUTE_NAME:
      case 'default_name':
        return $link->getUrl();

      case 'thumbnail_uri':
        return $this->getThumbnail();

      default:
        return parent::getMetadata($media, $attribute_name);
    }
  }

  /**
   * Gets the thumbnail image URI based on a link entity.
   *
   * @return string
   *   File URI of the thumbnail image or NULL if there is no specific icon.
   */
  protected function getThumbnail() {
    $path = \Drupal::service('extension.list.module')->getPath('medialink');
    $thumbnail = $path . '/images/icons/link.png';
    return is_file($thumbnail) ? $thumbnail : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    $field = parent::createSourceField($type);
    $settings = [
      'title' => FALSE,
      'link_type' => LinkItemInterface::LINK_EXTERNAL,
    ];
    return $field->set('settings', $settings);
  }

}
