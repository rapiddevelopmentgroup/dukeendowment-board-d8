<?php

namespace Drupal\duke_users;

use Drupal\monolog\Logger\MonologLoggerChannelFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\user\UserInterface;
use Drupal\node\Entity\Node;

/**
 * Class DefaultService.
 */
class DefaultService {

  /**
   * Drupal\monolog\Logger\MonologLoggerChannelFactory definition.
   *
   * @var \Drupal\monolog\Logger\MonologLoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new DefaultService object.
   */
  public function __construct(MonologLoggerChannelFactory $logger_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->loggerFactory = $logger_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Ensures a user profile exists and is associated to a user profile.
   *
   * @param Drupal\user\UserInterface $user
   *   The user to ensure a user profile node for.
   */
  public function ensureUserProfile(UserInterface $user) {
    if ($user->field_user_display_profile->isEmpty()) {

      $profile_title = isset($user->field_user_last_name->value)
        ? ($user->field_user_first_name->value . ' ' . $user->field_user_last_name->value)
        : $user->getDisplayName();

      $profiles = $this->entityTypeManager->getStorage('node')->loadByProperties(['title' => $profile_title]);
      $display_profile = reset($profiles);

      $this->loggerFactory->get('duke_users')
        ->notice('Creating a new user display profile');

      if (!$display_profile) {
        $display_profile = Node::create([
          'type' => 'display_profile',
          'title' => $profile_title,
          'langcode' => 'en',
          'uid' => '1',
          'status' => 1,
          'field_user_bio' => [
            'value' => $user->field_user_bio->value,
            'format' => 'wysiwyg',
          ],
        ]);
      }

      if (!$display_profile->field_user_title->value) {
        $display_profile->field_user_title->value = $user->field_user_title->value;
      }
      if (!$display_profile->field_user_cell_num->value) {
        $display_profile->field_user_cell_num->value = $user->field_user_cell_num->value;
      }
      if (!$display_profile->field_user_first_name->value) {
        $display_profile->field_user_first_name->value = $user->field_user_first_name->value;
      }
      if (!$display_profile->field_user_last_name->value) {
        $display_profile->field_user_last_name->value = $user->field_user_last_name->value;
      }
      if (!$display_profile->field_user_office_num->value) {
        $display_profile->field_user_office_num->value = $user->field_user_office_num->value;
      }

      if (!$user->field_user_picture->isEmpty()) {
        $display_profile->field_user_picture = [
          'target_id' => $user->field_user_picture->target_id,
          'alt' => 'User Profile',
        ];
      }

      $display_profile->save();

      $user->field_user_display_profile->entity = $display_profile;
    }
  }

  /**
   * Extra actions during user logout.
   *
   * @param Drupal\Core\Session\AccountProxy $user
   *   The user to logout.
   */
  public function logout(AccountProxy $user) {
    // nada.
  }

}
