<?php

namespace Drupal\duke_users;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\user\UserInterface;
use GuzzleHttp\Exception\BadResponseException;

class OpenIDConnect {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger factory used for logging.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The the file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  public function __construct(
    ClientInterface $http_client,
    LoggerChannelFactoryInterface $logger_factory,
    FileSystemInterface $file_system
  ) {
    $this->httpClient = $http_client;
    $this->loggerFactory = $logger_factory;
    $this->fileSystem = $file_system;
  }

  /**
   * Implements hook_openid_connect_claims_alter().
   */
  public function claims_alter(&$claims) {
    $claims['jobTitle'] = [
      'scope' => 'profile',
      'title' => 'Custom Title',
      'type' => 'string',
      'description' => 'Custom Title',
    ];

    $claims['physicalDeliveryOfficeName'] = [
      'scope' => 'profile',
      'title' => 'Custom Office Phone',
      'type' => 'string',
      'description' => 'Custom Office Phone',
    ];

    $claims['telephoneNumber'] = [
      'scope' => 'profile',
      'title' => 'Custom Mobile Phone',
      'type' => 'string',
      'description' => 'Custom Mobile Phone',
    ];

    $claims['thumbnailPhoto'] = [
      'scope' => 'profile',
      'title' => 'Thumbnail Photo',
      'type' => 'string',
      'description' => 'Thumbnail Photo',
    ];
  }

  /**
   * Implements hooks_openid_connect_userinfo_alter().
   */
  public function userinfo_alter(&$userinfo, &$context) {
    if ($context['plugin_id'] == 'windows_aad') {
      $access_token = $context['tokens']['access_token'];

      // Perform the request.
      $options = [
        'method' => 'GET',
        'headers' => [
          'Content-Type' => 'application/json',
          'Authorization' => 'Bearer ' . $access_token,
        ],
      ];
      $client = $this->httpClient;

      $url = 'https://graph.windows.net/me/thumbnailPhoto?api-version=1.6';

      // $this->loggerFactory->get('duke_users')
      //   ->notice('attempting to grab thumbnailPhoto');

      try {
        $response = $client->get($url, $options);
        $response_data = (string) $response->getBody();

        // $this->loggerFactory->get('duke_users')
        //   ->notice('successfully grabbed thumbnailPhoto');

        // $this->loggerFactory->get('duke_users')
        //   ->notice('response', ['response_data' => $response_data]);

        $filename = $this->fileSystem->saveData($response_data, 'public://');

        $userinfo['thumbnailPhoto'] = $filename;
      }
      catch (BadResponseException $e) {
        $variables = [
          '@message' => 'Could not retrieve user photo',
          '@error_message' => $e->getMessage(),
        ];
        $this->loggerFactory->get('duke_users')
          ->notice('@message. Details: @error_message', $variables);
      }
    }
  }

  /**
   * Implements hook_openid_connect_userinfo_save().
   *
   * @param \Drupal\user\UserInterface $account
   *   A user account object.
   * @param array $context
   *   An associative array with context information:
   *   - tokens:         Array of original tokens.
   *   - user_data:      Array of user and session data from the ID token.
   *   - userinfo:       Array of user information from the userinfo endpoint.
   *   - plugin_id:      The plugin identifier.
   *   - sub:            The remote user identifier.
   *   - is_new:         Whether the account was created during authorization.
   */
  public function userinfo_save(UserInterface $account, array $context) {
    $this->loggerFactory->get('duke_users')
      ->debug('Checking if new user via SSO: ' . $context['sub']);

    if ($context['is_new']) {
      $account->addRole('staff');
      // $account->save();
      $this->loggerFactory->get('duke_users')
        ->debug('New User via SSO, add staff role.');
    }
  }

}
