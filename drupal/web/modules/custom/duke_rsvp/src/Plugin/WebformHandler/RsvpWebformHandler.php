<?php

namespace Drupal\duke_rsvp\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformElementHelper;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Webform submission debug handler.
 *
 * @WebformHandler(
 *   id = "rsvp",
 *   label = @Translation("RSVP"),
 *   category = @Translation("RSVP"),
 *   description = @Translation("Conditionally handles RSVP webform submission."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class RsvpWebformHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $data = $webform_submission->getData();
    $meeting_nid = $webform_submission->entity_id->value;
    WebformElementHelper::convertRenderMarkupToStrings($data);
    $attending = stripos($data['will_attend'], 'yes') !== FALSE;
    $route_options = [
      'query' => [
        'attending' => $attending,
      ],
    ];
    $this->messenger()->deleteByType(MessengerInterface::TYPE_STATUS);
    if (!$attending) {
      $this->messenger()->addMessage($this->t('Thanks you for submitting. We will miss seeing you at the meeting.'));
    }
    else {
      $this->messenger()->addMessage($this->t('Thank you for submitting your RSVP! We look forward to seeing you at this meeting.'));
    }
    if (is_int($meeting_nid)) {
      $form_state->setRedirect('entity.node.canonical', ['node' => $meeting_nid], $route_options);
    }
  }

}
