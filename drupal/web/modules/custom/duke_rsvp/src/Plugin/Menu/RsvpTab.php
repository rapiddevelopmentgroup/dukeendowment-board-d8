<?php

namespace Drupal\duke_rsvp\Plugin\Menu;

use Drupal\Core\Menu\LocalTaskDefault;

/**
 * Provides customizations to the RSVP node tab.
 */
class RsvpTab extends LocalTaskDefault {

}
