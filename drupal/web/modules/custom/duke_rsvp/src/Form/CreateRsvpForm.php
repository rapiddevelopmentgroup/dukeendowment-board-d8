<?php

namespace Drupal\duke_rsvp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform\Entity\Webform;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Url;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class CreateRsvpForm.
 */
class CreateRsvpForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\node\NodeInterface Meeting Node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $meetingNode;

  /**
   * Constructs a new CreateRsvpForm object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Title callback for form page.
   */
  public function getTitle(NodeInterface $node = NULL) {
    return $this->t('Create RSVP form for @meeting', [
      '@meeting' => $node->getTitle(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'create_rsvp_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {
    $this->meetingNode = $node;

    if (isset($this->meetingNode->field_meeting_rsvp_webform) && !$this->meetingNode->field_meeting_rsvp_webform->isEmpty()) {
      \Drupal::messenger()->addMessage($this->t('RSVP form already created, you may edit the components as necessary below.'));
      return new RedirectResponse(
        Url::fromRoute(
          'entity.webform.edit_form',
          ['webform' => $this->meetingNode->field_meeting_rsvp_webform->target_id],
          []
        )->toString()
      );
    }

    $webform_template = Webform::load('rsvp_master');
    $elements = $webform_template->getElementsDecoded();

    $default_location = '';
    if ($address = $this->meetingNode->field_meeting_location->getValue()) {
      $default_location = $this->t('%street%street2, %city, %state', [
        '%street' => $address[0]['address_line1'],
        '%street2' => $address[0]['address_line2'] ? (' ' . $address[0]['address_line2']) : '',
        '%city' => $address[0]['locality'],
        '%state' => $address[0]['administrative_area'],
      ]);
    }

    $meeting_date = strtotime($this->meetingNode->field_meeting_date->value);
    $end_meeting_date = strtotime($this->meetingNode->field_meeting_date->end_value);
    $date_range_string = date('F j', $meeting_date) . ' – ' . date('j', $end_meeting_date);
    $date_options = [];
    do {
      $date_string = date('Y-m-d', $meeting_date);
      $date_options[$date_string] = $date_string;
      $meeting_date = strtotime('+1 days', $meeting_date);
    } while ($meeting_date <= $end_meeting_date);

    // Will attend meeting.
    $form['will_attend'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Trustee Meeting Invite'),
      '#default_value' => TRUE,
    ];

    $form['will_attend_description'] = [
      '#type' => 'textfield',
      '#default_value' => $this->t($elements['will_attend']['#title'], [
        '@date' => $date_range_string,
        '@name' => $this->meetingNode->getTitle(),
      ]),
      '#max_length' => 250,
      '#states' => [
        'visible' => [
          ':input[name="will_attend"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    // Will attend meetings.
    $form['will_attend_committee_meetings'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Committee Meeting Attendance'),
      '#default_value' => TRUE,
    ];

    $form['will_attend_committee_meetings_description'] = [
      '#type' => 'textfield',
      '#default_value' => $this->t($elements['will_attend_the_following']['#title'], [
        '@date' => $date_range_string,
        '@name' => $this->meetingNode->getTitle(),
      ]),
      '#max_length' => 250,
      '#states' => [
        'visible' => [
          ':input[name="will_attend_committee_meeetings"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    // Will attend reception.
    $form['will_attend_reception'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reception'),
      '#default_value' => TRUE,
    ];

    $form['will_attend_reception_description'] = [
      '#type' => 'textfield',
      '#default_value' => $this->t($elements['will_attend_reception']['#title'], [
        '@date' => $date_range_string,
        '@name' => $this->meetingNode->getTitle(),
      ]),
      '#max_length' => 250,
      '#states' => [
        'visible' => [
          ':input[name="will_attend_reception"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    // Will attend other event.
    $form['will_attend_additional_event'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Additional Dinner or Event'),
      '#default_value' => FALSE,
    ];

    $form['will_attend_additional_event_description'] = [
      '#type' => 'textfield',
      '#default_value' => $this->t($elements['will_attend_additional_event']['#title'], [
        '@date' => $date_range_string,
        '@name' => $this->meetingNode->getTitle(),
      ]),
      '#max_length' => 250,
      '#states' => [
        'visible' => [
          ':input[name="will_attend_additional_event"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    // Will need overnight accommodations.
    $form['will_need_overnight_accommodations'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reception'),
      '#default_value' => TRUE,
    ];

    $form['will_need_overnight_accommodations_description'] = [
      '#type' => 'textfield',
      '#default_value' => $this->t($elements['will_need_overnight_accommodations']['#title'], [
        '@date' => $date_range_string,
        '@name' => $this->meetingNode->getTitle(),
      ]),
      '#max_length' => 250,
      '#states' => [
        'visible' => [
          ':input[name="will_need_overnight_accommodations"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    // Will need airport transportation.
    $form['will_need_transportation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Airport Transportation'),
      '#default_value' => TRUE,
    ];

    $form['will_need_transportation_description'] = [
      '#type' => 'textfield',
      '#default_value' => $this->t($elements['will_need_transportation']['#title'], [
        '@date' => $date_range_string,
        '@name' => $this->meetingNode->getTitle(),
      ]),
      '#max_length' => 250,
      '#states' => [
        'visible' => [
          ':input[name="will_need_transportation"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    // Special Instructions
    $form['special_instructions'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Special Instructions'),
      '#default_value' => TRUE,
    ];

    $form['special_instructions_description'] = [
      '#type' => 'textfield',
      '#default_value' => $this->t($elements['special_instructions']['#title'], [
        '@date' => $date_range_string,
        '@name' => $this->meetingNode->getTitle(),
      ]),
      '#max_length' => 250,
      '#states' => [
        'visible' => [
          ':input[name="special_instructions"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 100,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#name' => 'submit',
      '#value' => $this->t('Create RSVP Form... return to meeting'),
    ];
    $form['actions']['submit_edit'] = [
      '#type' => 'submit',
      '#name' => 'submit_and_edit',
      '#value' => $this->t('Create RSVP Form... continue editing'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $webform_id = 'meeting_' . $this->meetingNode->id() . '_rsvp';
    $rsvp_form = Webform::load($webform_id);
    $webform_template = Webform::load('rsvp_master');
    $elements = NULL;
    if (!$rsvp_form) {
      $rsvp_form = $webform_template->createDuplicate();
      $rsvp_form->set('id', $webform_id);
      $rsvp_form->setElements($webform_template->getElementsDecoded());
    }

    $rsvp_form->set('title', $this->meetingNode->getTitle() . ' RSVP');
    $rsvp_form->set('description', 'RSVP form for ' . $this->meetingNode->getTitle());
    $rsvp_form->set('status', $this->meetingNode->isPublished() ? WebformInterface::STATUS_OPEN : WebformInterface::STATUS_CLOSED);

    $elements = $webform_template->getElementsDecoded();

    $sessions = \Drupal::service('duke_display.default')->sessionsForMeeting($this->meetingNode);
    $committee_sessions = array_filter($sessions, function ($session) {
      return isset($session->field_session_committees) && !$session->field_session_committees->isEmpty();
    });
    $board_sessions = array_filter($sessions, function ($session) {
      return !isset($session->field_session_committees) || $session->field_session_committees->isEmpty();
    });
    $committee_session_titles = array_map(function ($session) {
      return $session->getTitle();
    }, $committee_sessions);
    $board_session_titles = array_map(function ($session) {
      return $session->getTitle();
    }, $board_sessions);

    if ($form_state->getValue('will_attend')) {
      $elements['will_attend']['#title'] = $form_state->getValue('will_attend_description');
    }
    else {
      unset($elements['will_attend']);
    }

    if ($form_state->getValue('will_attend_committee_meetings')) {
      $elements['will_attend_the_following']['#title'] = $form_state->getValue('will_attend_committee_meetings_description');
      if ($board_session_titles) {
        $elements['will_attend_the_following']['board_meetings']['#options'] = array_combine($board_session_titles, $board_session_titles);
      }
      else {
        unset($elements['will_attend_the_following']['board_meetings']);
      }
      if ($committee_session_titles) {
        $elements['will_attend_the_following']['committee_meetings']['#options'] = array_combine($committee_session_titles, $committee_session_titles);
      }
      else {
        unset($elements['will_attend_the_following']['committee_meetings']);
      }
    }
    else {
      unset($elements['will_attend_committee_meetings']);
    }

    if ($form_state->getValue('will_attend_reception')) {
      $elements['will_attend_reception']['#title'] = $form_state->getValue('will_attend_reception_description');
    }
    else {
      unset($elements['will_attend_reception']);
    }

    if ($form_state->getValue('will_attend_additional_event')) {
      $elements['will_attend_additional_event']['#title'] = $form_state->getValue('will_attend_additional_event_description');
    }
    else {
      unset($elements['will_attend_additional_event']);
    }

    if ($form_state->getValue('will_need_overnight_accommodations')) {
      $elements['will_need_overnight_accommodations']['#title'] = $form_state->getValue('will_need_overnight_accommodations_description');
    }
    else {
      unset($elements['will_need_overnight_accommodations']);
    }

    if ($form_state->getValue('will_need_transportation')) {
      $elements['will_need_transportation']['#title'] = $form_state->getValue('will_need_transportation_description');
    }
    else {
      unset($elements['will_need_transportation']);
    }

    if ($form_state->getValue('special_instructions')) {
      $elements['special_instructions']['#title'] = $form_state->getValue('special_instructions_description');
    }
    else {
      unset($elements['special_instructions']);
    }

    $rsvp_form->setElements($elements);
    $rsvp_form->save();

    $this->meetingNode->field_meeting_rsvp_webform->target_id = $webform_id;
    $this->meetingNode->save();

    if ($trigger && $trigger['#name'] == 'submit_and_edit') {
      $form_state->setRedirect('entity.webform.edit_form', ['webform' => $webform_id]);
    }
    else {
      $form_state->setRedirect('entity.node.canonical', ['node' => $this->meetingNode->id()]);
    }

    // \Drupal::messenger()->addMessage($this->t('RSVP form updated.'));
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    $node = \Drupal::service('duke_display.default')->ensureMeetingNode();
    return AccessResult::allowedIf(isset($node) && $node->getType() == 'meeting' && $account->hasPermission('create rsvp form'));
  }

}
