<?php

namespace Drupal\duke_rsvp;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\webform\WebformInterface;

/**
 * Class DefaultService.
 */
class DefaultService {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new DefaultService object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Check if the user has a form submission.
   *
   * @param \Drupal\webform\WebformInterface|null $webform
   *   (optional) A webform. If set the total number of submissions for the
   *   Webform will be returned.
   *
   * @return int
   *   Total number of submissions.
   */
  public function userHasSubmission(WebformInterface $webform) {
    $current_account = \Drupal::currentUser()->getAccount();
    $submission_storage = $this->entityTypeManager->getStorage('webform_submission');
    $total = $submission_storage->getTotal($webform, NULL, $current_account);
    return $total > 0;
  }

}
