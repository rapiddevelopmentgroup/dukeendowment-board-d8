<?php

/**
 * @file
 */

$config_directories = [];

$settings['hash_salt'] = 'P6Effnzfv_KZyxoPTiwbCkVLB-GW1ZmkzxdOLRTdPxX4B38BDJwpeKtocAod1gkXISw73lYiHQ';
$settings['update_free_access'] = FALSE;
$settings['file_private_path'] = 'sites/default/files/private';
$settings['class_loader_auto_detect'] = FALSE;
$settings['container_yamls'][] = __DIR__ . '/services.yml';
$settings['googlemaps_api_key'] = 'AIzaSyBgNHOTFpjRZR0PbisBkyHYWCPZQSH-s68';
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

$settings['install_profile'] = 'minimal';
$settings['config_sync_directory'] = '../conf/sync';

$databases['default']['default'] = [
  'driver' => 'mysql',
  'database' => getenv('DB_NAME'),
  'username' => getenv('DB_USER'),
  'password' => getenv('DB_PASSWORD'),
  'host' => getenv('DB_HOST'),
  'prefix' => '',
  'port' => '',
];

// Include local settings. These come last so that they can override anything.
$on_platformsh = !empty($_ENV['PLATFORM_PROJECT']);
if ($on_platformsh && file_exists(__DIR__ . '/settings.platformsh.php')) {
  require_once __DIR__ . '/settings.platformsh.php';
}
elseif (file_exists(__DIR__ . '/settings.development.php')) {
  require_once __DIR__ . '/settings.development.php';
}
