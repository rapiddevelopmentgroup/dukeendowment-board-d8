<?php

assert_options(ASSERT_ACTIVE, TRUE);
\Drupal\Component\Assertion\Handle::register();

// Enforce MailHog intervention.
$config['smtp.settings']['smtp_on'] = FALSE;
$config['mailsystem.settings']['defaults']['sender'] = 'php_mail';
$config['system.mail']['interface']['default'] = 'php_mail';

$settings['container_yamls'][] = __DIR__ . '/development.services.yml';
$config['system.logging']['error_level'] = 'verbose';
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['discovery_migration'] = 'cache.backend.memory';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
$settings['extension_discovery_scan_tests'] = TRUE;
$settings['rebuild_access'] = TRUE;
$settings['skip_permissions_hardening'] = TRUE;

// $config_directories['migrate'] = 'modules/custom/duke_migration/config/install';
// $config_directories['field_type'] = 'modules/custom/rdg_migrate_field_type/config/install';

// $databases['import']['default'] = [
//   'driver' => 'mysql',
//   'database' => getenv('DB_NAME'),
//   'username' => getenv('DB_USER'),
//   'password' => getenv('DB_PASSWORD'),
//   'host' => getenv('IMPORT_DB_HOST'),
//   'prefix' => '',
//   'port' => '',
// ];

$config['search_api.server.search']['backend_config']['connector'] = 'standard';
$config['search_api.server.search']['backend_config']['connector_config']['scheme'] = 'http';
$config['search_api.server.search']['backend_config']['connector_config']['host'] = 'solr';
$config['search_api.server.search']['backend_config']['connector_config']['port'] = '8983';
$config['search_api.server.search']['backend_config']['connector_config']['path'] = '/';
$config['search_api.server.search']['backend_config']['connector_config']['core'] = 'search';
$config['search_api.server.search']['backend_config']['connector_config']['solr_install_dir'] = '../../..';
