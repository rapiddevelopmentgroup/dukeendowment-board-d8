(function ($, Drupal) {
  "use strict";

  // Mobile Menu
  Drupal.behaviors.mobileMenuBehavior = {
    attach: function (context) {
      $(context)
        .find("#toggle-nav")
        .click(function (e) {
          e.preventDefault();

          if ($(this).attr("aria-expanded") === "false") {
            $(this).attr("aria-expanded", "true");
            $("body").addClass("show-nav");
          } else {
            $(this).attr("aria-expanded", "false");
            $("body").removeClass("show-nav");
          }
        });
    },
  };

  // Fancy Select Boxes
  Drupal.behaviors.selectBoxes = {
    attach: function (context) {
      $("select", context).selectability();
    },
  };

  Drupal.behaviors.toggleBehavior = {
    attach: function (context) {
      // throttling variables
      var timeout = null;
      var delay = 100;
      var wait = function (afterward) {
        clearTimeout(timeout);
        timeout = setTimeout(afterward, delay);
      };

      // Resizing if statement to call search and account menu toggling functionality on desktop only.
      var windowResize = function () {
        if ($(window).width() > 767) {
          // Search Toggle
          $(
            "#search-form-wrapper input, #search-form-wrapper .selectability"
          ).attr("tabindex", -1);
          $(context)
            .find("#search-toggle")
            .click(function () {
              if ($(this).attr("aria-expanded") === "false") {
                $(this).attr("aria-expanded", "true");
                $("body").addClass("show-search");
                $("#search-form-wrapper .form-text").focus();
                $(
                  "#search-form-wrapper input, #search-form-wrapper .selectability"
                ).attr("tabindex", 0);
              } else {
                $(this).attr("aria-expanded", "false");
                $("body").removeClass("show-search");
                $(
                  "#search-form-wrapper input, #search-form-wrapper .selectability"
                ).attr("tabindex", -1);
              }
            });

          // Account Menu Toggle
          $("#block-useraccountmenu a").attr("tabindex", -1);

          $(context)
            .find("#toggle-account")
            .click(function (e) {
              e.preventDefault();

              if ($(this).attr("aria-expanded") === "false") {
                $(this).attr("aria-expanded", "true");
                $("#block-useraccountmenu a").attr("tabindex", 0);
                $("body").addClass("show-account");
                $("#block-useraccountmenu li:first-child a").focus();
              } else {
                $(this).attr("aria-expanded", "false");
                $("#block-useraccountmenu a").attr("tabindex", -1);
                $("body").removeClass("show-account");
              }
            });

          $(context)
            .find("#search-form-wrapper .form-submit")
            .keydown(function (e) {
              if (e.which === 9 && e.shiftKey) {
                // do nothing
              } else if (e.which === 9) {
                $("body").removeClass("show-search");
                $("#search-toggle").attr("aria-expanded", "false");
                $(
                  "#search-form-wrapper input, #search-form-wrapper .selectability"
                ).attr("tabindex", -1);
              }
            });

          $(context)
            .find("#block-useraccountmenu li:last-child a")
            .keydown(function (e) {
              if (e.which === 9 && e.shiftKey) {
                // do nothing
              } else if (e.which === 9) {
                $("#toggle-account").attr("aria-expanded", "false");
                $("#block-useraccountmenu a").attr("tabindex", -1);
                $("body").removeClass("show-account");
              }
            });
        } else {
          $(
            "#search-form-wrapper input, #search-form-wrapper .selectability, #block-useraccountmenu a"
          ).attr("tabindex", 0);

          $(context)
            .find("#search-form-wrapper .form-submit")
            .blur(function () {
              $("body").removeClass("show-nav");
              $("#toggle-nav").attr("aria-expanded", "false");
            });

          $(context)
            .find("#search-form-wrapper .form-submit")
            .keydown(function (e) {
              if (e.which === 9 && e.shiftKey) {
                // do nothing
              } else if (e.which === 9) {
                $("body").removeClass("show-nav");
                $("#toggle-nav").attr("aria-expanded", "false");
              }
            });
        }
      };

      // Call functionality on load.
      windowResize();

      // Call functionality again on window resize with the specified delay throttle.
      if (once("searchBehavior", "html").length) {
        $(window).resize(function () {
          wait(windowResize);
        });
      }
    },
  };

  Drupal.behaviors.loginFormBehavior = {
    attach: function (context) {
      $(".user-login-form .form-item", context)
        .find(".form-text")
        .on("focus change blur", function () {
          if ($(this).val().trim() === "") {
            $(this).parent().removeClass("active");
          } else {
            $(this).parent().addClass("active");
          }
        })
        .on("focus", function () {
          $(this).parent().addClass("active");
        });
    },
  };

  Drupal.behaviors.scrollToTop = {
    attach: function (context) {
      // throttling variables
      var delay = 100;
      var throttle = function (afterward) {
        setTimeout(afterward, delay);
      };

      var $button = $(context).find("#scroll-to-top");

      var showScrollToTop = function () {
        if ($(window).scrollTop() > 100) {
          $button.addClass("visible");
          $button.attr("tabindex", 0);
        } else {
          $button.removeClass("visible");
          $button.attr("tabindex", -1);
        }
      };

      // Call functionality again on window resize with the specified delay throttle.
      if (once("scrollToTop", "html").length) {
        $(window).scroll(function () {
          throttle(showScrollToTop);
        });
      }

      $button.click(function (e) {
        e.preventDefault();

        $("html, body").animate(
          {
            scrollTop: 0,
          },
          2000
        );
      });
    },
  };
})(jQuery, Drupal);
